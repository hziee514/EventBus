package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2022/2/24 16:36
 */
enum  ImmediateExecutor implements EventExecutor {

    INSTANCE;

    @Override
    public String threadMode() {
        return ThreadMode.IMMEDIATE;
    }

    @Override
    public void execute(Runnable command) {
        command.run();
    }

}
