package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2023/3/17 19:39
 */
public class EventExecutors {

    private EventExecutors() {}

    public static EventExecutor immediate() {
        return ImmediateExecutor.INSTANCE;
    }

    public static EventExecutor newThread() {
        return NewThreadExecutor.INSTANCE;
    }

    public static EventExecutor single() {
        return SingleExecutor.INSTANCE;
    }

    public static EventExecutor pool() {
        return PoolExecutor.INSTANCE;
    }

    public static EventExecutor ui() {
        return DelegateUiExecutor.INSTANCE;
    }

}
