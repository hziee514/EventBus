package top.fullj.eventbus;

import java.lang.reflect.Method;

/**
 * @author bruce.wu
 * @since 2022/2/24 15:13
 */
class Subscriber {

    final Object target;

    final Method method;

    final Class<?> eventType;

    final String threadMode;

    final boolean once;

    final boolean sticky;

    Subscriber(Object target, Method method) {
        this.target = target;
        this.method = method;
        this.eventType = method.getParameterTypes()[0];
        Subscribe subscribe = method.getAnnotation(Subscribe.class);
        this.threadMode = subscribe.thread();
        this.once = subscribe.once();
        this.sticky = subscribe.sticky();
    }

    @Override
    public int hashCode() {
        return (31 + method.hashCode()) * 31 + System.identityHashCode(target);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Subscriber) {
            Subscriber other = (Subscriber) obj;
            return target == other.target && method.equals(other.method);
        }
        return false;
    }
}
