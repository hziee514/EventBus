package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2022/2/24 15:10
 */
public interface ThreadMode {

    /**
     * execute in current thread immediately
     */
    String IMMEDIATE = "immediate";

    /**
     * execute in work thread pool
     */
    String THREAD_POOL = "threadPool";

    /**
     * execute in UI thread
     */
    String UI = "ui";

    /**
     * execute in new thread
     */
    String NEW_THREAD = "newThread";

    /**
     * execute in single thread(FIFO)
     */
    String SINGLE = "single";

}
