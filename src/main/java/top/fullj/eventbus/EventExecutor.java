package top.fullj.eventbus;

import java.util.concurrent.Executor;

/**
 * @author bruce.wu
 * @since 2022/3/29 12:23
 */
public interface EventExecutor extends Executor {

    String threadMode();

}
