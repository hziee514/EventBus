package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2022/5/13 19:36
 */
interface StickySubscribeHandler {

    void call(Subscriber subscriber);

}
