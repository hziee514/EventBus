package top.fullj.eventbus;

import java.io.Serializable;

/**
 * @author bruce.wu
 * @since 2022/10/22 22:21
 */
public class EventObject implements Serializable {

    private static final long serialVersionUID = 3326769492891147091L;

    private final transient Object source;

    private final long timestamp;

    public EventObject(Object source) {
        this(source, System.currentTimeMillis());
    }

    public EventObject(Object source, long timestamp) {
        if (source == null)
            throw new IllegalArgumentException("null source");
        this.source = source;
        this.timestamp = timestamp;
    }

    public Object getSource() {
        return source;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return getClass().getName() + "[source=" + source + ",timestamp=" + timestamp + "]";
    }

}
