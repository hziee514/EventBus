package top.fullj.eventbus;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author bruce.wu
 * @since 2022/3/4 9:48
 */
public class NamedThreadFactory implements ThreadFactory {

    private final ThreadGroup group;
    private final AtomicInteger index = new AtomicInteger(1);
    private final String prefix;

    public NamedThreadFactory(String tag) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
        prefix = tag + "#";
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r,
                prefix + index.getAndIncrement(),
                0);
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }

}
