package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2022/2/25 10:23
 */
public class DeadEvent extends EventObject {

    private static final long serialVersionUID = 705068683449901610L;

    private final Object event;

    public DeadEvent(Object source, Object event) {
        super(source);
        this.event = event;
    }
    
    public Object getEvent() {
        return this.event;
    }

}
