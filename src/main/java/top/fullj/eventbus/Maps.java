package top.fullj.eventbus;

import java.util.Map;

/**
 * @author bruce.wu
 * @since 2024/6/26 9:41
 */
public abstract class Maps {

    public static <K, V> V putIfAbsent(Map<K, V> map, K key, V value) {
        V v = map.get(key);
        if (v == null) {
            v = map.put(key, value);
        }
        return v == null ? value : v;
    }

}
