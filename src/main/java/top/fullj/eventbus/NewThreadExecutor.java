package top.fullj.eventbus;

import java.util.concurrent.ThreadFactory;

/**
 * @author bruce.wu
 * @since 2022/2/24 16:47
 */
enum NewThreadExecutor implements EventExecutor {

    INSTANCE;

    private static final ThreadFactory factory = new NamedThreadFactory("EBusThread");

    @Override
    public String threadMode() {
        return ThreadMode.NEW_THREAD;
    }

    @Override
    public void execute(Runnable command) {
        factory.newThread(command).start();
    }

}
