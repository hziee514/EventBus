package top.fullj.eventbus;

import java.lang.annotation.*;

/**
 * @author bruce.wu
 * @since 2022/4/11 16:34
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EventListener {

    /**
     * stop class(exclude) to stop search event listener method
     */
    Class<?> stopClass() default Object.class;

}
