package top.fullj.eventbus;

/**
 * @author bruce.wu
 * @since 2023/8/7 13:44
 */
public interface OnExecuteAssembly {

    Runnable apply(Runnable r);

}
