package top.fullj.eventbus;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author bruce.wu
 * @since 2022/3/29 12:30
 */
class SingleExecutor extends ThreadPoolExecutor implements EventExecutor {

    static final SingleExecutor INSTANCE = new SingleExecutor();

    private SingleExecutor() {
        super(0, 1,
                30L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(128),
                new NamedThreadFactory("EBusSingle"));
        allowCoreThreadTimeOut(true);
    }

    @Override
    public String threadMode() {
        return ThreadMode.SINGLE;
    }

}
