package top.fullj.eventbus;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author bruce.wu
 * @since 2022/3/29 12:30
 */
class PoolExecutor extends ThreadPoolExecutor implements EventExecutor {

    private static final int MAX_POOL_SIZE;
    private static final int CORE_POOL_SIZE;

    static {
        CORE_POOL_SIZE = Math.max(4, Runtime.getRuntime().availableProcessors());
        MAX_POOL_SIZE = CORE_POOL_SIZE * 2;
    }

    static final PoolExecutor INSTANCE = new PoolExecutor();

    private PoolExecutor() {
        super(CORE_POOL_SIZE, MAX_POOL_SIZE,
                30L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(64),
                new NamedThreadFactory("EBusPool"));
        allowCoreThreadTimeOut(true);
        System.out.println("EventBus.PoolExecutor: corePoolSize=" + CORE_POOL_SIZE + ", maxPoolSize=" + MAX_POOL_SIZE);
    }

    @Override
    public String threadMode() {
        return ThreadMode.THREAD_POOL;
    }

}
