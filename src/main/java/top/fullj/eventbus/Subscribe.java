package top.fullj.eventbus;

import java.lang.annotation.*;

/**
 * @author bruce.wu
 * @since 2022/2/24 15:02
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Subscribe {

    /**
     * which thread to handle the event
     */
    String thread() default ThreadMode.IMMEDIATE;

    /**
     * auto unsubscribe after dispatch the first event
     */
    boolean once() default false;

    /**
     * subscribe sticky event
     */
    boolean sticky() default false;

}
