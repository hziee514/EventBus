package top.fullj.eventbus;

import java.lang.reflect.Method;

/**
 * @author bruce.wu
 * @since 2022/2/24 16:41
 */
public interface ErrorHandler {

    void onError(EventBus bus, Object event, Object subscriber, Method method, Throwable tr);

}
