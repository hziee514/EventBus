package top.fullj.eventbus;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

/**
 * @author bruce.wu
 * @since 2022/2/25 10:22
 */
public class EventBusTest {

    private static final Logger LOG = LoggerFactory.getLogger(EventBusTest.class);

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
        //System.setProperty(PoolExecutor.KEY_CORE_POOL_SIZE, "1");
        //System.setProperty(PoolExecutor.KEY_MAX_POOL_SIZE, "4");
    }

    @Test
    public void testHandlerException() {
        Object listener = new Listener() {
            @Subscribe
            public void onEvent(Event1 e) {
                throw new RuntimeException("not impl");
            }
        };
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new Event1());
        EventBus.getDefault().unregister(listener);
    }

    @Test
    public void testUnsubscribedEvent() {
        Object listener = new Listener() {
            @Subscribe
            public void onEvent(EventObject e) {
            }
        };
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new Event1());
    }

    @Test
    public void testDeadEvent() {
        final AtomicInteger i = new AtomicInteger(0);
        final Event1 evt = new Event1();
        Object listener = new Listener() {
            @Subscribe
            public void onEvent(DeadEvent e) {
                assertEquals(evt, e.getEvent());
                i.incrementAndGet();
            }
        };
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(evt);
        assertEquals(1, i.get());
    }

    @Test
    public void testAbstractEventHandler() {
        EventBus.getDefault().register(new EventListener());
        EventBus.getDefault().post(new Event2());
    }

    @Test
    public void testThreadPoolEvent() throws Exception {
        Listener listener = new Listener() {
            @Subscribe(thread = ThreadMode.THREAD_POOL)
            public void onEvent(ThreadPoolEvent e) {
                LOG.info("onEvent: {}", e);
                /*try {
                    Thread.sleep(1);
                } catch (InterruptedException ignore) {

                }*/
            }
        };
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        Thread.sleep(50);
        EventBus.getDefault().post(new ThreadPoolEvent());
        Thread.sleep(31000);
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        Thread.sleep(50);
        EventBus.getDefault().post(new ThreadPoolEvent());
        Thread.sleep(31000);
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        EventBus.getDefault().post(new ThreadPoolEvent());
        Thread.sleep(100);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotListener() {
        EventBus.getDefault().register(new Listener() {});
    }

    @Test
    public void testSubscribeOnce() {
        final int count = 5;
        final AtomicInteger i = new AtomicInteger(0);
        Listener listener = new Listener() {
            @Subscribe(once = true)
            public void onEvent(Event1 e) {
                LOG.info("onEvent: {}", e);
                i.incrementAndGet();
            }
        };
        EventBus.getDefault().register(listener);
        for (int j = 0; j<count; j++) {
            EventBus.getDefault().post(new Event1());
        }
        assertEquals(1, i.get());
    }

    @Test
    public void testNonInheritListener() throws Exception {
        Object listener = new NonInheritEventListener();
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new Event2());
        EventBus.getDefault().post(new Event1());
        Thread.sleep(100);
    }

    @Test
    public void testSubNonInheritEventListener() throws Exception {
        EventBus.getDefault().register(new SubNonInheritEventListener());
        EventBus.getDefault().post(new Event2());
        EventBus.getDefault().post(new Event1());
        Thread.sleep(100);
    }

    @Test
    public void testInheritListener() throws Exception {
        Object listener = new InheritEventListener();
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new Event2());
        EventBus.getDefault().post(new Event1());
        Thread.sleep(100);
    }

    @Test
    public void testStickyEvents() {
        StickyEvent event = new StickyEvent();
        EventBus.getDefault().postSticky(event);
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                System.out.println(e);
                assertEquals(event, e);
            }
        });
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Test
    public void testConcurrencyStickyEvents() throws Exception {
        StickyEvent event = new StickyEvent();
        EventBus.getDefault().postSticky(event);
        final Object listener = new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                System.out.println(Thread.currentThread().getName() + ":" + e);
                assertEquals(event, e);
            }
        };
        final int threads = 8;
        CyclicBarrier barrier = new CyclicBarrier(threads);
        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + ": begin...");
                try {
                    barrier.await();
                    EventBus.getDefault().register(listener);
                    EventBus.getDefault().register(listener);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.out.println(Thread.currentThread().getName() + ": end...");
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
        System.out.println("end");
    }

    @Test
    public void testAddRemoveStickyEvents() {
        StickyEvent event1 = new StickyEvent();
        StickyEvent event2 = new StickyEvent();
        EventBus.getDefault().postSticky(event1);
        EventBus.getDefault().postSticky(event2);
        EventBus.getDefault().removeStickyEvent(event1);
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                System.out.println(e);
                assertEquals(event2, e);
            }
        });
    }

    @Test
    public void testAddRemoveStickyEvents1() {
        StickyEvent event1 = new StickyEvent();
        StickyEvent event2 = new StickyEvent();
        EventBus.getDefault().postSticky(event1);
        EventBus.getDefault().postSticky(event2);
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                System.out.println(e);
                assertTrue(Arrays.asList(event1, event2).contains(e));
            }
        });
        EventBus.getDefault().removeStickyEvents(StickyEvent.class);
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                fail("unexpected");
            }
        });
    }

    @Test
    public void testAddRemoveStickyEvents2() {
        StickyEvent event1 = new StickyEvent();
        Sticky2Event event2 = new Sticky2Event();
        EventBus.getDefault().postSticky(event1);
        EventBus.getDefault().postSticky(event2);
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                assertEquals(event1, e);
            }
            @Subscribe(sticky = true)
            public void onSticky2Event(Sticky2Event e) {
                assertEquals(event2, e);
            }
        });
        EventBus.getDefault().removeStickyEvents();
        EventBus.getDefault().register(new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(StickyEvent e) {
                fail("unexpected");
            }
            @Subscribe(sticky = true)
            public void onSticky2Event(Sticky2Event e) {
                fail("unexpected");
            }
        });
    }

    public static abstract class BaseListener {

        @Subscribe
        public void onDeadEvent(DeadEvent e) {
            System.out.println("onDeadEvent: " + e.getEvent());
        }

        @Subscribe
        public abstract void onEvent(Event2 e);

    }

    public static class EventListener extends BaseListener {

        //@Subscribe(ThreadMode.THREAD_POOL)
        @Subscribe(thread = ThreadMode.IMMEDIATE)
        public void onEvent(Event1 e) {
            throw new RuntimeException("not impl");
        }

        @Override
        public void onEvent(Event2 e) {
            System.out.println("onEvent: " + e);
        }
    }

    @top.fullj.eventbus.EventListener(stopClass = BaseListener.class)
    public static class NonInheritEventListener extends BaseListener {
        @Override
        public void onEvent(Event2 e) {
            System.out.println("onEvent: " + e);
        }
        @Subscribe
        public void onEvent(Event1 e) {
            System.out.println("onEvent: " + e);
        }
    }

    public static class SubNonInheritEventListener extends NonInheritEventListener {

    }

    @top.fullj.eventbus.EventListener
    public static class InheritEventListener extends BaseListener {
        @Override
        public void onEvent(Event2 e) {
            System.out.println("onEvent: " + e);
        }
        @Subscribe
        public void onEvent(Event1 e) {
            System.out.println("onEvent: " + e);
        }
    }

    public static class Event1 {
        @Override
        public String toString() {
            return "Event1";
        }
    }

    public static class Event2 {

    }

    public interface Listener {

    }

    public static class ThreadPoolEvent {

    }

    public static class Event {

        private final String name;
        private final long ts;

        public Event() {
            this.name = getClass().getSimpleName();
            this.ts = System.nanoTime();
        }

        @Override
        public String toString() {
            return name + ": " + ts;
        }
    }

    public static class StickyEvent extends Event {

    }

    public static class Sticky2Event extends Event {

    }

}