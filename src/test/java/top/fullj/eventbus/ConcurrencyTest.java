package top.fullj.eventbus;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

/**
 * @author bruce.wu
 * @since 2022/5/20 10:20
 */
public class ConcurrencyTest {

    @Test
    public void testConcurrencyRegister() throws Exception {
        AtomicInteger count = new AtomicInteger();
        final Object listener1 = new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(EventBusTest.StickyEvent e) {
                System.out.println(Thread.currentThread().getName() + ":" + e);
                count.incrementAndGet();
            }
        };
        final Object listener2 = new Object() {
            @Subscribe(sticky = true)
            public void onStickyEvent(EventBusTest.StickyEvent e) {
                System.out.println(Thread.currentThread().getName() + ":" + e);
                count.incrementAndGet();
            }
        };
        final int threads = 8;
        CyclicBarrier barrier = new CyclicBarrier(threads);
        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + ": begin...");
                try {
                    barrier.await();
                    EventBus.getDefault().register(listener1);
                    EventBus.getDefault().register(listener2);
                    System.out.println(Thread.currentThread().getName() + ": registered...");

                    EventBusTest.StickyEvent e = new EventBusTest.StickyEvent();
                    barrier.await();
                    EventBus.getDefault().postSticky(e);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    System.out.println(Thread.currentThread().getName() + ": end...");
                    latch.countDown();
                }
            }).start();
        }
        latch.await();
        EventBus.getDefault().removeStickyEvents(EventBusTest.StickyEvent.class);
        EventBus.getDefault().unregister(listener1);
        EventBus.getDefault().unregister(listener2);
        System.out.println("end");
        assertEquals(threads * 2, count.get());
    }

}
