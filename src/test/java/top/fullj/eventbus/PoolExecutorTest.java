package top.fullj.eventbus;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * @author bruce.wu
 * @since 2023/4/26 10:53
 */
public class PoolExecutorTest {

    @Test
    public void test() throws Exception {
        PoolExecutor executor = PoolExecutor.INSTANCE;
        final int COUNT = 18;
        final CountDownLatch latch = new CountDownLatch(COUNT);
        for (int i = 0; i < COUNT; i++) {
            executor.submit(() -> {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ignore) {

                }
                System.out.println(Thread.currentThread().getName() + ": "  + System.currentTimeMillis());
                latch.countDown();
            });
        }
        latch.await();
    }

}