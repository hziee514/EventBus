package top.fullj.eventbus;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author bruce.wu
 * @since 2024/6/27 9:54
 */
public class EventBusPluginTest {

    private static final Logger LOG = LoggerFactory.getLogger(EventBusPluginTest.class);

    private static final String KEY = "hello";

    private static class MDCPlugin implements Runnable {

        private final Map<String, String> ctx;
        private final Runnable r;

        MDCPlugin(Runnable r) {
            this.ctx = MDC.getCopyOfContextMap();
            this.r = r;
        }

        @Override
        public void run() {
            final Map<String, String> origin = MDC.getCopyOfContextMap();
            if (ctx != null) {
                MDC.setContextMap(ctx);
            }
            LOG.info("before: MDC.{}={}", KEY, MDC.get(KEY));
            r.run();
            if (origin == null) {
                MDC.clear();
            } else {
                MDC.setContextMap(origin);
            }
            LOG.info("after: MDC.{}={}", KEY, MDC.get(KEY));
        }
    }

    private static class ErrorHandlerPlugin implements ErrorHandler {
        private final CountDownLatch latch;
        ErrorHandlerPlugin(CountDownLatch latch) {
            this.latch = latch;
        }
        @Override
        public void onError(EventBus bus, Object event, Object subscriber, Method method, Throwable tr) {
            LOG.error("invoke {}.{}({}) failed",
                    subscriber.getClass().getSimpleName(),
                    method.getName(), event, tr.getCause());
            latch.countDown();
        }
    }

    @Test
    public void onExecuteAssembly() throws InterruptedException {
        final String val = "world";
        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicReference<String> ref = new AtomicReference<>();
        final Object listener = new Object() {
            @Subscribe(thread = ThreadMode.NEW_THREAD)
            public void onEvent(EventObject e) {
                LOG.info("onEvent: {}", e.getTimestamp());
                ref.set(MDC.get(KEY));
                latch.countDown();
            }
        };
        EventBusPlugins.setOnExecuteAssembly(MDCPlugin::new);
        MDC.put(KEY, val);
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new EventObject(this));
        if (!latch.await(2, TimeUnit.SECONDS)) {
            fail();
        }
        assertEquals(val, ref.get());
    }

    @Test
    public void errorHandler() throws InterruptedException {
        final Object listener = new Object() {
            @Subscribe(thread = ThreadMode.NEW_THREAD)
            public void onEvent(EventObject e) {
                throw new RuntimeException("stub!");
            }
        };
        final CountDownLatch latch = new CountDownLatch(1);
        EventBusPlugins.setErrorHandler(new ErrorHandlerPlugin(latch));
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new EventObject(this));
        if (!latch.await(2, TimeUnit.SECONDS)) {
            fail();
        }
    }

}
