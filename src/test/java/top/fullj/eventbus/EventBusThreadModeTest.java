package top.fullj.eventbus;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

/**
 * @author bruce.wu
 * @since 2024/6/27 9:35
 */
public class EventBusThreadModeTest {

    private static final Logger LOG = LoggerFactory.getLogger(EventBusThreadModeTest.class);

    @Test
    public void threadMode() throws InterruptedException {
        final int threadModes = 5;
        final AtomicInteger i = new AtomicInteger(0);
        final CountDownLatch latch = new CountDownLatch(threadModes);
        final Thread thread = Thread.currentThread();
        final Object listener = new Object() {
            @Subscribe
            private void onEventImmediate(EventObject e) {
                LOG.info("onEventImmediate: {}", e.getTimestamp());
                assertEquals(thread, Thread.currentThread());
                i.incrementAndGet();
                latch.countDown();
            }
            @Subscribe(thread = ThreadMode.UI)
            private void onEventUi(EventObject e) {
                LOG.info("onEventUi: {}", e.getTimestamp());
                assertTrue(Thread.currentThread().getName().startsWith("AWT-EventQueue-"));
                i.incrementAndGet();
                latch.countDown();
            }
            @Subscribe(thread = ThreadMode.SINGLE)
            private void onEventSingle(EventObject e) {
                LOG.info("onEventSingle: {}", e.getTimestamp());
                assertTrue(Thread.currentThread().getName().startsWith("EBusSingle#"));
                i.incrementAndGet();
                latch.countDown();
            }
            @Subscribe(thread = ThreadMode.THREAD_POOL)
            private void onEventPool(EventObject e) {
                LOG.info("onEventPool: {}", e.getTimestamp());
                assertTrue(Thread.currentThread().getName().startsWith("EBusPool#"));
                i.incrementAndGet();
                latch.countDown();
            }
            @Subscribe(thread = ThreadMode.NEW_THREAD)
            private void onEventNewThread(EventObject e) {
                LOG.info("onEventNewThread: {}", e.getTimestamp());
                assertTrue(Thread.currentThread().getName().startsWith("EBusThread#"));
                i.incrementAndGet();
                latch.countDown();
            }
        };
        EventBus.getDefault().register(listener);
        EventBus.getDefault().post(new EventObject(this));
        if (!latch.await(2, TimeUnit.SECONDS)) {
            fail();
        }
        assertEquals(threadModes, i.get());
    }

}
